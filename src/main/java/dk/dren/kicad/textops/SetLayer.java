package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;
import dk.dren.kicad.pcb.Node;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SetLayer implements TextOperation {
    private final String layerName;

    @Override
    public void accept(FpText fpText) {
        fpText.getPropertyValue("layer").setValue(layerName);
    }
}
