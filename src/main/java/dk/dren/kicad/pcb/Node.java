package dk.dren.kicad.pcb;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This is the basic and most general s-expression node found in kicad_pcb files, they are slightly limited s-expressions
 * which almost all have the following property:
 * There's always at least one string as the first sub-expression, that's the node name or some times the type of node.
 *
 * The entire kicad_pcb document is made up of Nodes in a tree, this allows code to deal with the data it understands
 * while leaving the rest of the document untouched, just like xml, but with less verbosity.
 *
 * To be safe there are no methods on Node that allow modification of the data, to do that you must implement a specific
 * class that understands the underlying model and is able to modify it safely.
 */
public interface Node extends NodeOrValue {
    String getName();
    Collection<NodeOrValue> getAllChildren();

    default List<MutableValue> getAttributes() {
        return streamAttributes().collect(Collectors.toList());
    }

    default Stream<MutableValue> streamAttributes() {
        return getAllChildren().stream()
                .filter(c->c instanceof MutableValue)
                .map(c->(MutableValue)c);
    }

    default Stream<Node> streamChildNodes() {
        return getAllChildren().stream()
                .filter(c->c instanceof Node)
                .map(c->(Node)c);
    }

    default Collection<Node> getChildren() {
        return streamChildNodes().collect(Collectors.toList());
    }

    default Stream<Node> streamChildNodesByName(String name) {
        return streamChildNodes()
                .filter(c->c.getName().equals(name));
    }

    default Optional<Node> getFirstChildByName(String name) {
        return streamChildNodesByName(name).findFirst();
    }
}
