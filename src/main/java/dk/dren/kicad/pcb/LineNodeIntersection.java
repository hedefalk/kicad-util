package dk.dren.kicad.pcb;

import dk.dren.kicad.pcb.LineNode;
import dk.dren.kicad.primitive.Point;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.decimal4j.immutable.Decimal6f;

@RequiredArgsConstructor
@Value
public class LineNodeIntersection {
    public LineNodeIntersection(Point intersection, LineNode lineNode) {
        this(intersection, lineNode, Decimal6f.ZERO);
    }

    private final Point intersection;
    private final LineNode lineNode;
    private final Decimal6f fudge;
}
