package dk.dren.kicad.primitive;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class ModuleReference implements Comparable<ModuleReference> {
    private final static Pattern REF = Pattern.compile("(\\D+)(\\d+)");
    private final String prefix;
    private final int number;

    public ModuleReference(String textValue) {
        Matcher refMatcher = REF.matcher(textValue);
        if (!refMatcher.matches()) {
            throw new IllegalArgumentException("Cannot parse "+textValue);
        }
        prefix = refMatcher.group(1);
        number = Integer.parseInt(refMatcher.group(2));
    }

    public int diff(ModuleReference other) {
        if (!other.getPrefix().equals(prefix)) {
            throw new IllegalArgumentException("Cannot find difference between "+this+" and "+other+" the prefix differs");
        }
        return other.getNumber()-number;
    }

    public ModuleReference add(int diff) {
        return new ModuleReference(prefix, number+diff);
    }

    @Override
    public String toString() {
        return prefix+number;
    }

    @Override
    public int compareTo(ModuleReference o) {
        int pc = prefix.compareTo(o.prefix);
        if (pc != 0) {
            return pc;
        }
        return Integer.compare(number, o.number);
    }

}
