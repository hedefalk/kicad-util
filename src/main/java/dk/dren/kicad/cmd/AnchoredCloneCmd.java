package dk.dren.kicad.cmd;

import dk.dren.kicad.layoutclone.AnchoredLayout;
import dk.dren.kicad.layoutclone.CloneResult;
import dk.dren.kicad.layoutclone.Layout;
import dk.dren.kicad.layoutclone.LayoutCloner;
import dk.dren.kicad.pcb.Board;
import dk.dren.kicad.primitive.ModuleReference;
import lombok.Getter;
import picocli.CommandLine;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;

@Getter
@CommandLine.Command(name = "anchored-clone", description = "Clones a zone on the PCB that is anchored around a part to all the other anchor parts\n" +
        "All anchor components must be pre-placed and one of them must be in the template zone\n" +
        "All parts must come from hierachial sub-sheets where the annotations are set to start from the next 100 or 1000 series.")
public class AnchoredCloneCmd implements  Callable<Integer> {

    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @CommandLine.Parameters(description = "The list of component references to use as anchors for the clone operation")
    private Set<String> anchors = new TreeSet<>();

    @Override
    public Integer call() throws Exception {
        LayoutCloner layoutCloner = new LayoutCloner(pcbCmd.getBoard());

        if (anchors.size() < 2) {
            return Fail.it("The list of anchors must contain at least two module references,\n" +
                    "like this: pcb -f thingy.kicad_pcb anchored-clone IC101 IC201 IC301");
        }

        ModuleReference first = null;
        Set<ModuleReference> as = new TreeSet<>();
        for (String anchor : anchors) {
            ModuleReference mr;
            try {
                mr = new ModuleReference(anchor);
            } catch (IllegalArgumentException e) {
                return Fail.it("Could not parse the anchor reference "+anchor+": "+e.getMessage());
            }

            if (as.contains(mr)) {
                return Fail.it("The list of anchors must be unique");
            }

            if (first== null) {
                first = mr;
            } else {
                if (!mr.getPrefix().equals(first.getPrefix())) {
                    return Fail.it("All the anchor prefixes must be the same, the first one was "+first+" so the rest must also start with "+first.getPrefix()+"\n" +
                            "the reference "+mr+" does not have the correct prefix");
                }

                int diff = first.diff(mr);

                if (diff % 100 != 0) {
                    return Fail.it("All the anchor prefixes must refer to the same component in the hierachial sheet "+mr+
                            " if done correctly all anchors will have a number that's a whole 100 apart from "+first);
                }
            }

            if (layoutCloner.getBoard().getModuleWithReference(mr) == null) {
                return Fail.it("Unknown reference: "+mr);
            }

            as.add(mr);
        }

        Layout layout = new AnchoredLayout(layoutCloner.getBoard(), layoutCloner.getTemplateModules(), as);

        CloneResult cr = layoutCloner.cloneAndStore(layout);
        System.out.println(cr);

        return 0;
    }
}
