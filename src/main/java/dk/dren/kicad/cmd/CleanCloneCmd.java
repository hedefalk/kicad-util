package dk.dren.kicad.cmd;

import dk.dren.kicad.layoutclone.LayoutCloner;
import lombok.Getter;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(name = "clone-remove", description = "Removes previously created elements such as vias and segments")
public class CleanCloneCmd implements  Callable<Integer> {

    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @Override
    public Integer call() throws Exception {
        LayoutCloner layoutCloner = new LayoutCloner(pcbCmd.getBoard());

        int removed = layoutCloner.removeGeneratedElementsAndStore();
        if (removed >= 0) {
            System.out.println("Removed " + removed + " previously generated items");
        } else {
            System.out.println("Did not find the report file: "+layoutCloner.getReportFile());
        }

        return 0;
    }
}
