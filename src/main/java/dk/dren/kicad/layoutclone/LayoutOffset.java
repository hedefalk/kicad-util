package dk.dren.kicad.layoutclone;

import dk.dren.kicad.primitive.XYR;
import lombok.Value;

/**
 * The offset in position, angle and part reference compared from the template parts to the cloned parts.
 */
@Value
public class LayoutOffset {
    XYR xyr;
    int reference;
}
